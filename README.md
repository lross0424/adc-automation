This is a Repository for Scripts to configure Citrix ADC with Ansible or traditional SSH copy paste scripts.

### Direct NITRO API Calls
https://developer-docs.citrix.com/projects/netscaler-ansible-modules/en/latest/generic-modules/nitro-api-calls/

### ADC/Ansible Community
https://github.com/citrix/netscaler-ansible-modules

## NITRO 12.0 Documentation
https://developer-docs.citrix.com/projects/netscaler-nitro-api/en/12.0/
