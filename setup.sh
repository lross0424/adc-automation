#!/bin/bash
sudo apt-get update
sudo apt-get install python3-pip -y
git clone https://github.com/citrix/citrix-adc-ansible-modules.git
cp requirements.test.txt citrix-adc-ansible-modules/
cd citrix-adc-ansible-modules
sudo pip3 install -r requirements.test.txt
python3 install.py
cd ..
curl https://get.acme.sh | sh
